const express = require("express");
const router = express.Router();
const db = require("../db/connection");
const bcrypt = require('bcrypt');

router.post("/", (req, res) => {
  const obj = req.body;
  const cmd_select = "SELECT login FROM USUARIOS WHERE login = ?";

  db.query(cmd_select, obj.login, (error, row) => {
    if (error) {
      return res.status(500).json({ error })
    }
    if (row.length > 0) {
      return res.status(404).json({ message: "Usuário já existe" });
    } else {
      const senhacriptografada = bcrypt.hashSync(obj.password, 10) //"HASH 10" é utilizado para fazer a criptogtrafia mais complexa

      obj.password = senhacriptografada;

      const cmdinsert = "INSERT INTO USUARIOS SET ?";
      db.query(cmdinsert, obj, (error, result) => {
        if (error) {

          return res.status(404).json({ message: "Erro" + error });
        }
        else
          return res.status(201).json({ message: "Usuário Criado!" });
      });

    }
  });
});


router.get("/", (req, res) => {
  const cmd_selectAll = "SELECT * FROM USUARIOS";
  db.query(cmd_selectAll, (err, rows) => {
    if (err) {
      return res.status(505).json({ err })
    }
    res.status(200).json(rows);
  });
});

//**AUTENTICANDO**
router.post('/auth', (req, res) => {
  const { login, password } = req.body;

  if (!login || !password)
    return res.status(404).json({ message: "Dados insuficientes" });

  cmdSelect = "SELECT password FROM USUARIOS WHERE login = ?"
  db.query(cmdSelect, login, (error, row) => {
    if (row.length > 0) {

      const hash = bcrypt.compareSync(password, row[0].password) // compara com a senha que foi salva no banco
      if (hash)
        return res.status(200).json({ message: "OK" });
      else
        return res.status(401).json({ message: "Senha não confere" });
    };
  });
});

module.exports = router;


