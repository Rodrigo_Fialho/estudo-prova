const express = require("express");
const port = 8000;

const contatosRouter = require ('./Routes/contatos')
const usuariosRouter = require ('./Routes/usuarios')

const app = express()
app.use(express.json())


app.use('/contatos',contatosRouter)
app.use('/usuarios',usuariosRouter)



app.listen (port,()=>{
    console.log("-------------------------")
    console.log(`Rodando na porta ${port}`)
    console.log("-------------------------")
})
